# ---------------------------------------------------------------------------
# These show up as missing but idk what to do with them
weird_mounts = [
   459, # Gray Wolf
   468, # White Stallion
   578, # Black Wolf
   579, # Red Wolf
   581, # Winter Wolf
  6896, # Black Ram
  8980, # Skeletal Horse
 10795, # Ivory Raptor
 15780, # Green Mechanostrider
 18363, # Riding Kodo
 25863, # Black Qiraji Battle Tank
 26655, # Black Qiraji Battle Tank
 28828, # Nether Drake
 33630, # Blue Mechanostrider
 44317, # Merciless Nether Drake
 48778, # Acherus Deathcharger
 49378, # Brewfest Riding Kodo
 55164, # Swift Spectral Gryphon
 59572, # Black Polar Bear
 60136, # Grand Caravan Mammoth
 60140, # Grand Caravan Mammoth
 62048, # Black Dragonhawk Mount
 66122, # Magic Rooster
 66123, # Magic Rooster
 66124, # Magic Rooster
194046, # Swift Spectral Rylak
200175, # Felsaber
239363, # Swift Spectral Hippogryph
239766, # Blue Qiraji War Tank
239767, # Red Qiraji War Tank
239770, # Black Qiraji War Tank
]

# Mounts
mounts = (
    ('Both - Pandaren', (
        127295,# Great Black Dragon Turtle
        127302,# Great Blue Dragon Turtle
        127308,# Great Brown Dragon Turtle
        127293,# Great Green Dragon Turtle
        127310,# Great Purple Dragon Turtle
        120822,# Great Red Dragon Turtle
        127286,# Black Dragon Turtle
        127287,# Blue Dragon Turtle
        127288,# Brown Dragon Turtle
        120395,# Green Dragon Turtle
        127289,# Purple Dragon Turtle
        127290,# Red Dragon Turtle
    )),
    ('Both - PvP', (
        (23510, 23509), # Stormpike Battle Charger/Frostwolf Howler (Alterac Valley)
        (60118, 60119), # Black War Bear
        222240,# Prestigious Azure Courser
        222202,# Prestigious Bronze Courser
        222237,# Prestigious Forest Courser
        222238,# Prestigious Ivory Courser
        222241,# Prestigious Midnight Courser
        222236,# Prestigious Royal Courser
    )),
    ('Class', (
        13819, # Warhorse (Paladin)
        23214, # Charger (Paladin)
        73629, # Exarch's Elekk (Draenei Paladin)
        73630, # Great Exarch's Elekk (Draenei Paladin)
        34769, # Thalassian Warhorse (BE Paladin)
        34767, # Thalassian Charger (BE Paladin)
        69820, # Sunwalker Kodo (Tauren Paladin)
        69826, # Great Sunwalker Kodo (Tauren Paladin)
        66906, # Argent Charger (Paladin)
        5784,  # Felsteed (Warlock)
        23161, # Dreadsteed (Warlock)
        54729, # Winged Steed of the Ebon Blade (Death Knight)
    )),
    ('Garrison', (
        127271,# Crimson Water Strider (Fishing Shack)
        171626,# Armored Irontusk (Trading Post)
        171839,# Ironside Warwolf (Trading Post)
        189364,# Coalfist Gronnling (Mission)
        171826,# Mudback Riverbeast (Mission)
    )),
    ('Garrison - Invasion', (
        171836,# Garn Steelmaw
        171635,# Giant Coldsnout
        171624,# Shadowhide Pearltusk
        171843,# Smoky Direwolf
    )),
    ('Garrison - Stables', (
        171629,# Armored Frostboar
        171838,# Armored Frostwolf
        171617,# Trained Icehoof
        171623,# Trained Meadowstomper
        171638,# Trained Riverwallow
        171637,# Trained Rocktusk
        171831,# Trained Silverpelt
        171841,# Trained Snarler
    )),
    ('Guild', (
        (90621, 93644), # Golden King/Kor'kron Annihilator
        88990, # Dark Phoenix
        124408,# Thundering Jade Cloud Serpent
        171627,# Blacksteel Battleboar (Guild Glory of the Draenor Raider)
    )),
    ('Misc', (
        138642,# Black Primal Raptor
        138643,# Green Primal Raptor
        138641,# Red Primal Raptor
    )),
    ('Order Halls', (
        229387,# Deathlord's Vilebrood Vanquisher (Death Knight)
        229417,# Slayer's Felbroken Shrieker (Demon Hunter)
        # (Druid)?
        229439,# Huntmaster's Dire Wolfhawk (Hunter)
        229438,# Huntmaster's Fierce Wolfhawk (Hunter)
        229386,# Huntmaster's Loyal Wolfhawk (Hunter)
        229376,# Archmage's Prismatic Disc (Mage)
        229385,# Ban-Lu, Grandmaster's Companion (Monk)
        231435,# Highlord's Golden Charger (Paladin)
        231589,# Highlord's Valorous Charger (Paladin)
        231587,# Highlord's Vengeful Charger (Paladin)
        231588,# Highlord's Vigilant Charger (Paladin)
        229377,# High Priest's Lightsworn Seeker (Priest)
        231524,# Shadowblade's Baneful Omen (Rogue)
        231525,# Shadowblade's Crimson Omen (Rogue)
        231523,# Shadowblade's Lethal Omen (Rogue)
        231434,# Shadowblade's Murderous Omen (Rogue)
        231442,# Farseer's Raging Temptest (Shaman)
        238454,# Netherlord's Accursed Wrathsteed (Warlock)
        238452,# Netherlord's Brimstone Wrathsteed (Warlock)
        232412,# Netherlord's Chaotic Wrathsteed (Warlock)
        229388,# Battlelord's Bloodthirsty War Wyrm (Warrior)
    )),
    ('Puzzles', (
        223018,# Fathom Dweller (Kosumoth)
        215159,# Long-Forgotten Hippogryph (Azsuna crystals)
        247402,# Lucid Nightmare (weird puzzle?)
        243025,# Riddler's Mind-Worm (Gift of the Mind-Seekers)
    )),
    ('Quests', (
        138640,# Bone-White Primal Raptor
        75207, # Vashj'ir Seahorse
        #98718, # Subdued Seahorse (Poseidus)
        54753, # White Polar Bear
    )),
    ('Quests - Legion', (
        230987,# Arcanist's Manasaber (Suramar, Balance of Power chain)
        171850,# Llothien Prowler (random drop?)
        213164,# Brilliant Direbeak
        213158,# Predatory Bloodgazer
        213163,# Snowfeather Hunter
        213165,# Viridian Sharptalon
    )),
    ('World Events', (
        71342, # Big Love Rocket (Love is in the Air)
        230844,# Brawler's Burly Basilisk (Brawler's Guild)
        103081,# Darkmoon Dancing Bear (Darkmoon Faire Vendor)
        247448,# Darkmoon Dirigible (Darkmoon Faire)
        228919,# Darkwater Skate (Darkmoon Faire Vendor)
        49379, # Great Brewfest Kodo (Brewfest)
        48025, # Headless Horseman's Mount
        191314,# Minion of Grumpus
        43900, # Swift Brewfest Ram (Brewfest)
        102346,# Swift Forest Strider
        102350,# Swift Lovebird (Love is in the Air)
        102349,# Swift Springstrider
    )),
    ('Recruit-A-Friend', (
        49322, # Swift Zhevra
        75973, # X-53 Touring Rocket
        121820,# Obsidian Nightwing
        149801,# Emerald Hippogryph
        171847,# Cindermane Charger
    )),
    # ('Unknown - WoD', (
    #     116657,# Ancient Leatherhide  Unknown - WoD
    #     104011,# Stormcrow
    # )),
    ('Unknown - Legion', (
        # 137576,# Coldflame Infernal -??
        # 137615,# Flarecore Infernal -??
        # 137614,# Frostshard Infernal -??
        243201,# Demonic Gladiator's Storm Dragon
        227995,# Dominating Gladiator's Storm Dragon
        227994,# Fierce Gladiator's Storm Dragon
    )),

    # Alliance only
    ('#Alliance Only', ()),

    ('Allied Races', (
        258022, # Lightforged Felcrusher (Allied Races: Lightforged Draenei)
        259202, # Starcursed Voidstrider (Allied Races: Void Elf)
    )),
    ('Darnassus', (
        10789, # Spotted Frostsaber
        66847, # Striped Dawnsaber
        8394,  # Striped Frostsaber
        10793, # Striped Nightsaber
        23221, # Swift Frostsaber
        23219, # Swift Mistsaber
        23338, # Swift Stormsaber
    )),
    ('Exodar', (
        34406, # Brown Elekk
        35710, # Gray Elekk
        35711, # Purple Elekk
        35713, # Great Blue Elekk
        35712, # Great Green Elekk
        35714, # Great Purple Elekk
    )),
    ('Gilneas', (
        103195,# Mountain Horse
        103196,# Swift Mountain Horse
    )),
    ('Gnomeregan', (
        10969, # Blue Mechanostrider ?33630
        17453, # Green Mechanostrider ?15780
        10873, # Red Mechanostrider
        17454, # Unpainted Mechanostrider
        23225, # Swift Green Mechanostrider
        23223, # Swift White Mechanostrider
        23222, # Swift Yellow Mechanostrider
    )),
    ('Ironforge', (
        6899,  # Brown Ram
        6777,  # Gray Ram
        6898,  # White Ram
        23238, # Swift Brown Ram
        23239, # Swift Gray Ram
        23240, # Swift White Ram
    )),
    ('Stormwind', (
        470,   # Black Stallion
        458,   # Brown Horse
        6648,  # Chestnut Mare
        472,   # Pinto Bridle
        23229, # Swift Brown Steed
        23227, # Swift Palomino
        23228, # Swift White Steed
    )),
    ('Argent Tournament', (
        63637, # Darnassian Nightsaber
        65638, # Swift Moonsaber
        63639, # Exodar Elekk
        65637, # Great Red Elekk
        63638, # Gnomeregan Mechanostrider
        65642, # Turbostrider
        63636, # Ironforge Ram
        65643, # Swift Violet Ram
        63232, # Stormwind Steed
        65640, # Swift Gray Steed
    )),
    ('PvP', (
        22719, # Black Battlestrider
        48027, # Black War Elekk
        22720, # Black War Ram
        22717, # Black War Steed
        22723, # Black War Tiger

        223341,# Vicious Gilnean Warhorse
        229487,# Vicious War Bear (Alliance)
        223578,# Vicious War Elekk
        242896,# Vicious War Fox (Stormwind, PvP alliance)
        229512,# Vicious War Lion (Alliance)
        183889,# Vicious War Mechanostrider
        171834,# Vicious War Ram
        146615,# Vicious Warsaber
        100332,# Vicious War Steed
        232523,# Vicious War Turtle (Alliance)
    )),

    # Horde only
    ('#Horde Only', ()),

    ('Allied Races', (
        258060, # Highmountain Thunderhoof (Allied Races: Highmountain Tauren)
        258845, # Nightborne Manasaber (Allied Races: Nightborne)
    )),
    ('Bilgewater Cartel', (
        87090, # Goblin Trike Key
        87091, # Goblin Turbo-Trike Key
    )),
    ('Darkspear Trolls', (
        8395,  # Emerald Raptor
        10796, # Turqoise Raptor
        10799, # Violet Raptor
        23241, # Swift Blue Raptor
        23242, # Swift Olive Raptor
        23243, # Swift Orange Raptor
    )),
    ('Orgrimmar', (
        64658, # Black Wolf ?578
        6654,  # Brown Wolf
        6653,  # Dire Wolf
        580,   # Timber Wolf
        23250, # Swift Brown Wolf
        23252, # Swift Gray Wolf
        23251, # Swift Timber Wolf
    )),
    ('Silvermoon City', (
        35022, # Black Hawkstrider
        35020, # Blue Hawkstrider
        35018, # Purple Hawkstrider
        34795, # Red Hawkstrider
        35025, # Swift Green Hawkstrider
        33660, # Swift Pink Hawkstrider
        35027, # Swift Purple Hawkstrider
    )),
    ('Thunder Bluff', (
        18990, # Brown Kodo
        18989, # Gray Kodo
        64657, # White Kodo
        23249, # Great Brown Kodo
        23248, # Great Gray Kodo
        23247, # Great White Kodo
    )),
    ('Undercity', (
        17465, # Green Skeletal Warhorse
        66846, # Ochre Skeletal Warhorse
        23246, # Purple Skeletal Warhorse
        64977, # Black Skeletal Horse
        17463, # Blue Skeletal Horse
        17464, # Brown Skeletal Horse
        17462, # Red Skeletal Horse
    )),
    ('Argent Tournament', (
        63635, # Darkspear Raptor
        65644, # Swift Purple Raptor
        63640, # Orgrimmar Wolf
        65646, # Swift Burgundy Wolf
        63642, # Silvermoon Hawkstrider
        65639, # Swift Red Hawkstrider
        63641, # Thunder Bluff Kodo
        65641, # Great Golden Kodo
        63643, # Forsaken Warhorse
        65645, # White Skeletal Warhorse
    )),
    ('PvP', (
        22718, # Black War Kodo
        22721, # Black War Raptor
        22724, # Black War Wolf
        22722, # Red Skeletal Warhorse
        35028, # Swift Warstrider

        146622,# Vicious Skeletal Warhorse
        229486,# Vicious War Bear (Horde)
        242897,# Vicious War Fox (Orgrimmar, PvP horde)
        185052,# Vicious War Kodo
        171835,# Vicious War Raptor
        230988,# Vicious War Scorpion (Horde)
        223354,# Vicious War Trike
        232525,# Vicious War Turtle (Horde)
        100333,# Vicious War Wolf
        223363,# Vicious Warstrider
    )),

    # Achievements
    ('#Achievements', ()),

    ('Collect', (
        60025, # Albino Drake (50 mounts)
        (61996, 61997), # Blue/Red Dragonhawk (100 mounts)
        133023,# Jade Pandaren Kite (150 mounts)
        (142478, 142266), # Armored Blue/Red Dragonhawk (200 mounts)
        97501, # Felfire Hawk (250 mounts)
        127169,# Heavenly Azure Cloud Serpent (300 mounts)
        (179244, 179245),# Chauffeured Mechano-Hog/Chauffeured Mekgineer's Chopper (Heirloom Hoarder)
        223814,# Mechanized Lumber Extractor (300 toys)
    )),
    ('WotLK', (
        59961, # Red Proto-Drake
        63963, # Rusted Proto-Drake
        63956, # Ironbound Proto-Drake
        72808, # Bloodbathed Frostbrood Vanquisher
        72807, # Icebound Frostbrood Vanquisher
    )),
    ('Cata', (
        97359, # Flameward Hippogryph (Molten Front)
        88331, # Volcanic Stone Drake (Glory of the Cataclysm Hero)
        88335, # Drake of the East Wind (Glory of the Cataclysm Raider)
        97560, # Corrupted Fire Hawk (Glory of the Firelands Raider)
        107844,# Twilight Harbinger (Glory of the Dragon Soul Raider)
    )),
    ('MoP', (
        (130985, 118737), # Pandaren Kite String (Pandaren Ambassador)
        127156,# Crimson Cloud Serpent
        127161,# Heavenly Crimson Cloud Serpent
        136400,# Armored Skyscreamer
        148392,# Spawn of Galakras
    )),
    ('WoD', (
        191633,# Soaring Skyterror (Draenor Pathfinder)
        171632,# Frostplains Battleboar (Glory of the Draenor Hero)
        171436,# Gorestrider Gronnling (Glory of the Draenor Raider)
        186305,# Infernal Direwolf (Glory of the Hellfire Raider)
    )),
    ('Legion', (
        225765,# Leyfeather Hippogryph (Glory of the Legion Hero)
        193007,# Grove Defiler (Glory of the Legion Raider)
        253087,# Antoran Gloomhound (Glory of the Argus Raider)
        254260,# Bleakhoof Ruinstrider (...And Chew Mana Buns)
        215558,# Ratstallion (Underbelly Tycoon)
        (193695, 204166),# Prestigious War Steed/Prestigious War Wolf (Free For All, More For Me)
    )),
    ('Other', (
        60024, # Violet Proto-Drake (world events)
        142073,# Hearthsteed
        175700,# Emerald Drake (instance mounts)
    )),

    # Instances
    ('#Instances', ()),

    ('Classic', (
        17481, # Rivendare's Deathcharger (Stratholme)
        25953, # Blue Qiraji Battle Tank (AQ40)
        26056, # Green Qiraji Battle Tank (AQ40)
        26054, # Red Qiraji Battle Tank (AQ40)
        26055, # Yellow Qiraji Battle Tank (AQ40)
    )),
    ('TBC', (
        36702, # Fiery Warhorse (Kara)
        40192, # Ashes of Al'ar (TK)
        41252, # Raven Lord (Sethekk Halls)
        46628, # Swift White Hawkstrider (Magister's Terrace)
    )),
    ('WotLK', (
        59567, # Azure Drake (Malygos)
        59568, # Blue Drake (Malygos)
        59650, # Black Drake (OS)
        59571, # Twilight Drake (OS)
        (61465, 61467), # Grand Black War Mammoth (VoA)
        63796, # Mimiron's Head (Uld)
        69395, # Onyxian Drake (Ony)
        72286, # Invincible (ICC)
        59996, # Blue Proto-Drake (Utgarde Pinnacle)
        59569, # Bronze Drake (CoS)
        73313, # Crimson Deathcharger (ICC, Shadowmourne quest)
    )),
    ('Cata', (
        88742, # Drake of the North Wind (VP)
        88744, # Drake of the South Wind (ToFW)
        101542,# Flametalon of Alysrazor (FL)
        97493, # Pureblood Fire Hawk (FL)
        107842,# Blazing Drake (DS)
        110039,# Experiment 12-B (DS)
        107845,# Life-Binder's Handmaiden (DS)
        88746, # Vitreous Stone Drake (SC)
        98204, # Amani Battle Bear (ZA)
        96491, # Armored Razzashi Raptor (ZG)
        96499, # Swift Zulian Panther (ZG)
    )),
    ('MoP', (
        127170,# Astral Cloud Serpent (MSV)
        136471,# Spawn of Horridon (ToT)
        139448,# Clutch of Ji-Kun (ToT)
        148417,# Kor'kron Juggernaut (SoO)
    )),
    ('WoD', (
        171621,# Ironhoof Destroyer (BRF)
        182912,# Felsteel Annihilator (HFC)
        189999,# Grove Warden (HFC)
    )),
    ('Legion', (
        229499,# Midnight (Karazhan, Attumen)
        231428,# Smoldering Ember Wyrm (Karazhan, Nightbane)
        213134,# Felblaze Infernal (Gul'dan, Nighthold)
        171827,# Hellfire Infernal (Gul'dan mythic, Nighthold)
        232519,# Abyss Worm (Mistress Sassz'ine, Tomb of Sargeras)
        253088,# Antoran Charhound (Felhounds of Sargeras, Antorus)
        253660,# Biletooth Gnasher (Argus?)
        243651,# Shackled Ur'zul (Argus the Unmaker Mythic, Antorus)
    )),
    ('Other', (
        201098,# Infinite Timereaver (Timewalking)
    )),

    # Professions
    ('#Professions', ()),

    ('Alchemy', (
        93326, # Sandstone Drake (Alchemy)
    )),
    ('Archaeology', (
        84751, # Fossilized Raptor
        92155, # Scepter of Azj'Aqir
        196681,# Spirit of Eche'ro
    )),
    ('Blacksmithing', (
        213209,# Steelbound Devourer (Blacksmithing)
    )),
    ('Engineering', (
        (60424, 55531), # Mekgineer's Chopper/Mechano-Hog
        134359,# Sky Golem
        44151, # Turbo-Charged Flying Machine
        126507,# Depleted-Kyparium Rocket
        44153, # Flying Machine
        126508,# Geosynchronous World Spinner
    )),
    ('Fishing', (
        214791,# Brinedeep Bottom-Feeder (Conjurer Margoss - Honored, Fishing)
        253711,# Pond Nettle (Fishing)
        64731, # Sea Turtle (Fishing)
    )),
    ('Jewelcrafting', (
        120043,# Jeweled Onyx Panther
        121837,# Jade Panther
        121838,# Ruby Panther
        121836,# Sapphire Panther
        121839,# Sunstone Panther
    )),
    ('Leatherworking', (
        171844,# Dustmane Direwolf (Leatherworking)
        213339,# Great Northern Elderhorn (Leatherworking)
    )),
    ('Tailoring', (
        169952,# Creeping Carpet
        61451, # Flying Carpet
        75596, # Frosty Flying Carpet
        61309, # Magnificent Flying Carpet
    )),

    # Rare Spawns
    ('#Rare Spawns', ()),

    ('WotLK', (
        60002, # Time-Lost Proto-Drake
    )),
    ('Cata', (
        88750, # Grey Riding Camel (Uldum)
        88718, # Phosporescent Stone Drake (Deepholm)
        98718, # Subdued Seahorse (Vashj'ir)
    )),
    ('MoP', (
        138424,# Amber Primordial Direhorn (Zandalari Warbringer)
        138426,# Jade Primordial Direhorn (Zandalari Warbringer)
        138425,# Slate Primordial Direhorn (Zandalari Warbringer)
        127158,# Heavenly Onyx Cloud Serpent (Sha of Anger)
        130965,# Son of Galleon (Galleon)
        139442,# Thundering Cobalt Cloud Serpent (Nalak)
        148476,# Thundering Onyx Cloud Serpent (Huolon)
        138423, # Cobalt Primordial Direhorn (Oondasta)
    )),
    ('WoD', (
        171620,# Bloodhoof Bull
        171851,# Garn Nighthowl
        171636,# Great Greytusk
        171622,# Mottled Meadowstomper
        171824,# Sapphire Riverbeast
        171828,# Solar Spirehawk (World Boss - WoD)
        171849,# Sunhide Gronnling
        171830,# Swift Breezestrider
        179478,# Voidtalon of the Dark Star
        171630,# Armored Razorback (Tanaan big rares)
        171619,# Tundra Icehoof (Tanaan big rares)
        171837,# Warsong Direfang (Tanaan big rares)
    )),
    ('Legion', (
        235764,# Darkspore Mana Ray (Fel-Spotted Egg - Naroua/Sabuul/Varga)
        253108,# Felglow Mana Ray (Fel-Spotted Egg - Naroua/Sabuul/Varga)
        253107,# Lambent Mana Ray (Fel-Spotted Egg - Naroua/Sabuul/Varga)
        253109,# Scintillating Mana Ray (Fel-Spotted Egg - Naroua/Sabuul/Varga)
        253106,# Vibrant Mana Ray (Fel-Spotted Egg - Naroua/Sabuul/Varga)
        253662,# Acid Belcher (Skreeg the Devourer, Mac'Aree)
        253661,# Crimson Slavermaw (Blistermaw, Antoran Wastes)
        243652,# Vile Fiend (Houndmaster Kerrax, Antoran Wastes)
        253058,# Maddened Chaosrunner (Wrangler Kravos, Mac'Aree)
    )),
    
    # Reputation
    ('#Reputation', ()),

    ('Classic', (
        43927, # Cenarion War Hippogryph (Cenarion Expedtion)
        (17229, 64659), # Winterspring Frostsaber/Whistle of the Venomhide Ravasaur (Wintersaber Trainers/whatever dudes)
    )),
    ('TBC - Kurenai/Mag\'har', (
        39315, # Cobalt Riding Talbuk
        39317, # Silver Riding Talbuk
        39318, # Tan Riding Talbuk
        39319, # White Riding Talbuk
        34896, # Cobalt War Talbuk
        34898, # Silver War Talbuk
        34899, # Tan War Talbuk
        34897, # White War Talbuk
    )),
    ('TBC - Netherwing', (
        41514, # Azure Netherwing Drake
        41515, # Cobalt Netherwing Drake
        41513, # Onyx Netherwing Drake
        41516, # Purple Netherwing Drake
        41517, # Veridian Netherwing Drake
        41518, # Violet Netherwing Drake
    )),
    ('TBC - Sha\'tari Skyguard', (
        39803, # Blue Riding Nether Ray
        39798, # Green Riding Nether Ray
        39801, # Purple Riding Nether Ray
        39800, # Red Riding Nether Ray
        39802, # Silver Riding Nether Ray
    )),
    ('WotLK', (
        61294, # Green Proto-Drake (Oracles)
        (66087, 66088), # Silver Covenant Hippogryph/Sunreaver Dragonhawk (Silver Covenant)
        (66090, 66091), # Quel'dorei Steed/Sunreaver Hawkstrider (Silver Covenant)
        (61469, 61470), # Grand Ice Mammoth (Sons of Hodir)
        (59797, 59799), # Ice Mammoth (Sons of Hodir)
        59570, # Red Drake (Wyrmrest Accord)
    )),
    ('Cata', (
        88741, # Drake of the West Wind (Baradin's Wardens)
        (92231, 92232), # Spectral Steed/Spectral Wolf (Baradin's Wardens)
        88748, # Brown Riding Camel (Ramkahen)
        88749, # Tan Riding Camel (Ramkahen)
    )),
    ('MoP', (
        118089,# Azure Water Strider (Anglers)
        129918,# Thundering August Cloud Serpent (August Celestials)
        132036,# Thundering Ruby Cloud Serpent (August Celestials)
        127174,# Azure Riding Crane (Golden Lotus)
        127176,# Golden Riding Crane (Golden Lotus)
        127177,# Regal Riding Crane (Golden Lotus)
        123886,# Amber Scorpion (Klaxxi)
        130092,# Red Flying Cloud (Lorewalkers)
        123992,# Azure Cloud Serpent (Order of the Cloud Serpent)
        123993,# Golden Cloud Serpent (Order of the Cloud Serpent)
        113199,# Jade Cloud Serpent (Order of the Cloud Serpent)
        129934,# Blue Shado-Pan Riding Tiger (Shado-Pan)
        129932,# Green Shado-Pan Riding Tiger (Shado-Pan)
        129935,# Red Shado-Pan Riding Tiger (Shado-Pan)
        127154,# Oynx Cloud Serpent (Shado-Pan)
        130138,# Black Riding Goat (Tillers)
        130086,# Brown Riding Goat (Tillers)
        130137,# White Riding Goat (Tillers)
        (136163, 136164), # Grand Gryphon/Grand Wyvern (Operation: Shieldwall/Dominance Offensive)
        (135416, 135418), # Grand Armored Gryphon/Grand Armored Wyvern (Operation: Shieldwall/Dominance Offensive)
        (140249, 140250), # Golden/Crimson Primal Direhorn (Kirin Tor Offensive/Sunreaver Onslaught)
        127164, # Heavenly Golden Cloud Serpent (Emperor Shaohao)
    )),
    ('WoD', (
        171829,# Shadowmane Charger (Arakkoa Outcasts)
        (171625, None),# Dusty Rockhide (Council of Exarchs)
        (None, 171842),# Swift Frostwolf (Frostwolf Orcs)
        190690,# Bristling Hellboar (The Sabrestalkers)
        171633,# Wild Goretusk (The Sabrestalkers)
        171634,# Domesticated Razorback (Steamwheedle Preservation Society)
        190977,# Deathtusk Felboar (Vol'jin's Headhunters/Hand of the Prophets)
        (None, 171832),# Breezestrider Stallion (Vol'jin's Spear)
        (171833, None),# Pale Thorngrazer (Wrynn's Vanguard)
    )),
    ('Legion', (
        242881,# Cloudwing Hippogryph (Farondis Chest)
        242874,# Highmountain Elderhorn (Highmountain Supplies)
        230401,# Ivory Hawkstrider (Talon's Vengeance Exalted)
        233364,# Leywoven Flying Carpet (Nightfallen Cache)
        242882,# Valarjar Stormwing (Valarjar Strongbox)
        242875,# Wild Dreamrunner (Dreamweaver Cache)
    )),
    ('Legion - Argus', (
        253004,# Amethyst Ruinstrider (Argussian Reach, Exalted)
        253005,# Beryl Ruinstrider (Argussian Reach, Exalted)
        253007,# Cerulean Ruinstrider (Argussian Reach, Exalted)
        253006,# Russet Ruinstrider (Argussian Reach, Exalted)
        242305,# Sable Ruinstrider (Argussian Reach, Exaled)
        253008,# Umber Ruinstrider (Argussian Reach, Exalted)
        254259,# Avenging Felcrusher (Gleaming Footlocker - Army of the Light)
        254258,# Blessed Felcrusher (Gleaming Footlocker - Army of the Light)
        254069,# Glorious Felcrusher (Gleaming Footlocker - Army of the Light)
        239013,# Lightforged Warframe (Army of the Light, Exalted)
    )),

    # Vendor
    ('#Vendor', ()),

    ('Vendor', (
        (171846, None),# Champion's Treadblade (Alliance chopper thing)
        194464,# Eclipse Dragonhawk
        142910,# Ironbound Wraithcharger
        127165,# Yu'lei, Daughter of Jade (Timewalking)
    )),
    ('TBC', (
        39316, # Dark Riding Talbuk (Halaa)
        34790, # Dark War Talbuk (Halaa)
        (32242, 32296), # Swift Blue Gryphon/Swift Yellow Wind Rider
        (32290, 32295), # Swift Green Gryphon/Swift Green Wind Rider
        (32292, 32297), # Swift Purple Gryphon/Swift Purple Wind Rider
        (32289, 32246), # Swift Red Gryphon/Swift Red Wind Rider
        (32239, 32243), # Ebon Gryphon/Tawny Wind Rider
        (32235, 32245), # Golden Gryphon/Green Wind Rider
        (32240, 32244), # Snowy Gryphon/Blue Wind Rider
    )),
    ('WotLK', (
        (60114, 60116), # Armored Brown Bear (Dalaran)
        (61229, 61230), # Armored Snowy Gryphon/Armored Blue Wind Rider (Dalaran)
        (61447, 61425), # Traveler's Tundra Mammoth (Dalaran)
        (59791, 59793), # Wooly Mammoth (Dalaran)
        (59785, 59788), # Black War Mammoth
        63844, # Argent Hippogryph
        67466, # Argent Warhorse
    )),
    ('MoP', (
        122708,# Grand Expedition Yak (Kun-Lai Summit)
        127220,# Blonde Riding Yak (Kun-Lai Summit)
        127216,# Grey Riding Yak (Kun-Lai Summit)
        148428,# Ashhide Mushan Beast (Timeless Isle)
    )),
    ('WoD', (
        183117,# Corrupted Dreadwing (Apexis Crystals)
        171825,# Mosshide Riverwallow (Apexis Crystals)
        171628,# Rocktusk Battleboar (Gold - Garrison)
        171616,# Witherhide Cliffstomper (Gold - Garrison)
    )),
    ('Legion', (
        227956,# Arcadian War Turtle (Curious Coins)
        213115,# Bloodfang Widow (The Mad Merchant)
    )),

    # Unavailable
    ('#Unavailable', ()),

    ('Classic', (
        43688, # Amani War Bear (ZA)
        16056, # Ancient Frostsaber
        16081, # Arctic Wolf
        26656, # Black Qiraji Battle Tank (AQ40 opening) ?26655 26656
        16055, # Black Nightsaber
        17461, # Black Ram ?6896
        64656, # Blue Skeletal Warhorse
        43899, # Brewfest Ram
        17460, # Frost Ram
        18991, # Green Kodo
        17459, # Icy Blue Mechanostrider Mod A
        17450, # Ivory Raptor ?10795
        16082, # Palomino
        #13323, # Purple Mechanostrider
        #13324, # Red and Blue Mechanostrider
        16080, # Red Wolf
        66847, # Swift Dawnsaber
        24242, # Swift Razzashi Raptor (ZG)
        24252, # Swift Zulian Tiger (ZG)
        18992, # Teal Kodo
        16084, # Mottled Red Raptor
        15779, # White Mechanostrider Mod B
        16083, # White Stallion ?468
    )),
    ('WotLK', (
        68188, # Crusader's Black Warhorse (ToC)
        68187, # Crusader's White Warhorse (ToC)
        59976, # Black Proto-Drake (Naxx/etc 25 meta)
        60021, # Plagued Proto-Drake (Naxx/etc 10 meta)
        68057, # Swift Alliance Steed (ToC)
        68056, # Swift Horde Wolf (ToC)
    )),
    ('MoP', (
        142641,# Brawler's Burly Mushan Beast (Brawler's Guild)
        132117,# Ashen Pandaren Phoenix (CM)
        129552,# Crimson Pandaren Phoenix (CM)
        132118,# Emerald Pandaren Phoenix (CM)
        132119,# Violet Pandaren Phoenix (CM)
        148396,# Kor'kron War Wolf (Garrosh)
        (None, 171845), # Warlord's Deathwheel
    )),
    ('WoD', (
        171848,# Challenger's War Yeti (Challenge Warlord: Silver)
        170347,# Core Hound (Lv100 Molten Core)
    )),
    ('Legion', (
        253639,# Violet Spellwing (Blood of a Titan - kill Argus)
        272472,# Undercity Plaguebat - Burning of Teldrassil
        274610,# Teldrassil Hippogryph - Burning of Teldrassil
    )),
    ('Arena', (
        37015, # Swift Nether Drake
        44744, # Merciless Nether Drake ?44744
        49193, # Vengeful Nether Drake
        58615, # Brutal Nether Drake
        64927, # Deadly Gladiator's Frost Wyrm
        65439, # Furious Gladiator's Frost Worm
        67336, # Relentless Gladiator's Frost Wyrm
        71810, # Wrathful Gladiator's Frost Wyrm
        101282,# Vicious Gladiator's Twilight Drake
        101821,# Ruthless Gladiator's Twilight Drake
        124550,# Cataclysmic Gladiator's Twilight Drake
        139407,# Malevolent Gladiator's Cloud Serpent
        148618,# Tryannical Gladiator's Cloud Serpent
        148619,# Grievous Gladiator's Cloud Serpent
        148620,# Prideful Gladiator's Cloud Serpent
        186828,# Primal Gladiator's Felblood Gronnling (Warlords S1)
        189043,# Wild Gladiator's Felblood Gronnling (Warlords S2)
        189044,# Warmongering Gladiator's Felblood Gronnling (Warlords S3)
        227986,# Vindictive Gladiator's Storm Dragon (Legion S1)
        227988,# Fearless Gladiator's Storm Dragon (Legion S2)
        227989,# Cruel Gladiator's Storm Dragon (Legion S3)
        227991,# Ferocious Gladiator's Storm Dragon (Legion S4)
    )),
    ('Promotions', (
        58983, # Big Blizzard Bear (Blizzcon)
        (245723, 245725),# Stormwind Skychaser/Orgrimmar Interceptor (Blizzcon 2017)
        107203,# Tyrael's Charger (Annual Pass)
        (107516, 107517), # Spectral Gryphon/Spectral Wind Rider (Scroll of Resurrection)
        232405,# Primal Flamesaber (Heroes of the Storm)
    )),

    # $$$$$
    ('#$$$$$', ()),

    ('CE', (
        124659,# Imperial Quilen (MoP)
        155741,# Dread Raven (WoD)
        189998,# Illidari Felstalker (Legion)
        (255695, 255696), # Seabraid Stallion/Gilded Ravasaur (Battle for Azeroth Collector's Edition)
    )),
    ('Store', (
        139595,# Armored Bloodwing
        75614, # Celestial Steed
        142878,# Enchanted Fey Dragon
        163025,# Grinning Reaver
        110051,# Heart of the Aspects
        153489,# Iron Skyreaver
        243512,# Luminous Starseeker
        180545,# Mystic Runesaber
        259395,# Shu-zen, the Divine Sentinel
        134573,# Swift Windsteed
        163024,# Warforged Nightmare
        98727, # Winged Guardian
    )),
    ('TCG', (
        96503, # Amani Dragonhawk
        51412, # Big Battle Bear
        74856, # Blazing Hippogryph
        102514,# Corrupted Hippogryph
        113120,# Feldrake
        136505,# Ghastly Charger
        65917, # Magic Rooster ?66122 66123 66124
        93623, # Mottled Drake
        30174, # Riding Turtle
        97581, # Savage Raptor
        42776, # Spectral Tiger
        42777, # Swift Spectral Tiger
        101573,# Swift Shorestrider
        102488,# White Riding Camel
        74918, # Wooly White Rhino
        46197, # X-51 Nether-Rocket
        46199, # X-51 Nether-Rocket X-TREME
    )),
)

all_mounts = set()
for set_name, mount_ids in mounts:
    for mount_id in mount_ids:
        if isinstance(mount_id, int):
            all_mounts.add(mount_id)
        else:
            all_mounts.update(mount_id)
