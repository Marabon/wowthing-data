# ---------------------------------------------------------------------------
# Currency categories
currency_categories = {
    82: "Archaeology",
    23: "TBC",
    81: "Cata",
    4: "Classic",
    22: "Dung",
    141: "Legion",
    89: "Meta",
    1: "Misc",
    133: "MoP",
    2: "PvP",
    137: "WoD",
    21: "WotLK",
}

currency_category_order = [141, 137, 133, 81, 21, 23, 4, 22, 2, 89, 1]

# ---------------------------------------------------------------------------
# Currency highlights - [amount, css, text]
currency_highlights = {
    1508: [ # Veiled Argunite
        [650, 'exciting', 'Gamble a 910 item!'],
    ],
    1533: [ # Wakening Essence
        [1000, 'exciting', 'Gamble a random legendary!'],
        [300, 'highlight', 'Upgrade a legendary to item level 1000!'],
    ],
    124124: [ # Blood of Sargeras
        [20, 'exciting', 'Buy a 4000 Order Hall Resources BoA thingy'],
    ],
}

# ---------------------------------------------------------------------------

currency_items = [
    dict(id=124124, category=141, name='Blood of Sargeras', icon='inv_blood_of_sargeras'),
    dict(id=137642, category=2, name='Mark of Honor', icon='ability_pvp_gladiatormedallion'),
    dict(id=116415, category=1, name='Pet Charm', icon='achievement_guildperk_honorablemention'),
]

# ---------------------------------------------------------------------------

honor_per_level = [
    # First prestige level is a little weird
    [
        350, 350, 350, 350, 350, 350, 350, 350, 350, 350,
        400, 400, 400, 400, 400, 400, 400, 400, 400, 400,
        450, 450, 450, 450, 450, 450, 450, 450, 450, 450,
        500, 500, 500, 500, 500, 500, 500, 500, 500, 500,
        550, 550, 550, 550, 550, 550, 550, 550, 550, 550,
    ],
    # Every level after that is the same
    [
        800, 800, 800, 800, 800, 800, 800, 800, 800, 800,
        850, 850, 850, 850, 850, 850, 850, 850, 850, 850,
        900, 900, 900, 900, 900, 900, 900, 900, 900, 900,
        950, 950, 950, 950, 950, 950, 950, 950, 950, 950,
        1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000,
    ],
]
