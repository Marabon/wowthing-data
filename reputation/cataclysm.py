reputations_cataclysm = dict(
    name='Cataclysm',
    key='cata',
    reputations=[
        # 4.2
        [
            dict(id=1204, name='Avengers of Hyjal', icon='inv_neck_hyjaldaily_04'),
        ],
        [
            dict(
                alliance_id=1177,
                alliance_name="Baradin's Wardens",
                alliance_icon='inv_banner_tolbarad_alliance',
                horde_id=1178,
                horde_name="Hellscream's Reach",
                horde_icon='inv_banner_tolbarad_horde',
            ),
            dict(
                alliance_id=1174,
                alliance_name='Wildhammer Clan',
                alliance_icon='inv_misc_tabard_wildhammerclan',
                horde_id=1172,
                horde_name='Dragonmaw Clan',
                horde_icon='inv_misc_tabard_dragonmawclan',
            ),
            dict(id=1135, name='The Earthen Ring', icon='inv_misc_tabard_earthenring'),
            dict(id=1158, name='Guardians of Hyjal', icon='inv_misc_tabard_guardiansofhyjal'),
            dict(id=1173, name='Ramkahen', icon='inv_misc_tabard_tolvir'),
            dict(id=1171, name='Therazane', icon='inv_misc_tabard_therazane'),
        ],
    ],
)
