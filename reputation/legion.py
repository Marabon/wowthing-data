reputations_legion = dict(
    name='Legion',
    key='legion',
    reputations=[
        # 7.3 - Argus
        [
            dict(id=2170, name='Argussian Reach', icon='inv_legion_faction_argussianreach', paragon=True),
            dict(id=2165, name='Army of the Light', icon='inv_legion_faction_armyofthelight', paragon=True),
        ],
        # 7.2 - Broken Shore
        [
            dict(id=2045, name='Armies of Legionfall', icon='achievement_faction_legionfall', paragon=True),
        ],
        # 7.1 - Weird PvP traitor thing
        [
            dict(id=2018, name="Talon's Vengeance", icon='inv_falcosaurospet_white'),
        ],
        # 7.0
        [
            dict(id=1900, name='Court of Farondis', icon='inv_legion_faction_courtoffarnodis', paragon=True),
            dict(id=1883, name='Dreamweavers', icon='inv_legion_faction_dreamweavers', paragon=True),
            dict(id=1828, name='Highmountain Tribe', icon='inv_legion_faction_hightmountaintribes', paragon=True),
            dict(id=1859, name='The Nightfallen', icon='inv_legion_faction_nightfallen', paragon=True),
            dict(id=1948, name='Valarjar', icon='inv_legion_faction_valarjar', paragon=True),
            dict(id=1894, name='The Wardens', icon='inv_legion_faction_warden', paragon=True),
        ],
        # Friends
        [
            dict(id=1975, name='Conjurer Margoss', icon='inv_elemental_primal_water', friend=True),
        ],
        [
            dict(id=2099, name='Akule Riverhorn', icon='trade_archaeology_vrykul_runestick', friend=True),
            dict(id=2100, name='Corbyn', icon='inv_jewelcrafting_gem_27', friend=True),
            dict(id=2097, name='Ilyssia of the Waters', icon='ability_monk_forcesphere', friend=True),
            dict(id=2102, name='Impus', icon='ability_deathknight_desecratedground', friend=True),
            dict(id=2098, name='Keeper Raynae', icon='ability_xavius_corruptingnova', friend=True),
            dict(id=2101, name="Sha'leth", icon='inv_misc_fish_58', friend=True),
        ],
    ],
)
