faction_icons = {
    # Legion
    2170: 'inv_legion_faction_argussianreach',
    2165: 'inv_legion_faction_armyofthelight',
    2045: 'achievement_faction_legionfall',
    1900: 'inv_legion_faction_courtoffarnodis',
    1883: 'inv_legion_faction_dreamweavers',
    1828: 'inv_legion_faction_hightmountaintribes',
    1859: 'inv_legion_faction_nightfallen',
    1948: 'inv_legion_faction_valarjar',
    1894: 'inv_legion_faction_warden',

    1090: 'achievement_reputation_kirintor',
}

# ---------------------------------------------------------------------------
# Reputations to display
            # dict(
            #     alliance_id=0,
            #     alliance_name='',
            #     alliance_icon='',
            #     horde_id=0,
            #     horde_name='',
            #     horde_icon='',
            # ),
            # dict(id=0, name='', icon=''),

# ---------------------------------------------------------------------------
# Paragon rewards
paragon_rewards = {
    # Army of the Light
    2165: [
        ['mount', 254259, 'Avenging Felcrusher'], 
        ['mount', 254258, 'Blessed Felcrusher'],
        ['mount', 254069, 'Glorious Felcrusher'],
        ['toy', 153182, 'Holy Lightsphere'],
    ],
    # Armires of Legionfall
    2045: [
        ['pet', 121715, 'Orphaned Felbat'],
    ],
    # Court of Farondis
    1900: [
        ['mount', 242881, 'Cloudwing Hippogryph'],
    ],
    # Dreamweavers
    1883: [
        ['mount', 242875, 'Wild Dreamrunner'],
    ],
    # Highmountain Tribe
    1828: [
        ['mount', 242874, 'Highmountain Elderhorn'],
    ],
    # The Nightfallen
    1859: [
        ['mount', 233364, 'Leywoven Flying Carpet'],
    ],
    # Valarjar
    1948: [
        ['mount', 242882, 'Valarjar Stormwing'],
    ],
    # The Wardens
    1894: [
        ['toy', 147843, "Sira's Extra Cloak"],
    ],
}

# ---------------------------------------------------------------------------

reputation_levels = dict(
    reputation={
        0: 'Hated',
        1: 'Hostile',
        2: 'Unfriendly',
        3: 'Neutral',
        4: 'Friendly',
        5: 'Honored',
        6: 'Revered',
        7: 'Exalted',
        8: 'Paragon',
    },
    friend={
        0: 'Stranger',
        1: 'Pal',
        2: 'Buddy',
        3: 'Friend',
        4: 'Good Friend',
        5: 'Best Friend',
    },
    bodyguard={
        0: 'Bodyguard',
        1: 'Wingman',
        2: 'Personal Wingman',
    },
)
