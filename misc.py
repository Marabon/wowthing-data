import json
import os

from core.models import Currency, Instance, Realm

# ---------------------------------------------------------------------------

def relpath(f):
    return os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), f))

# ---------------------------------------------------------------------------

wowdb_base = 'www.wowdb.com'
wowhead_base = 'www.wowhead.com'

# ---------------------------------------------------------------------------
# Achievements
achievements = json.load(open(relpath('../../achievement.json')))['achievements']
criteria_map = json.load(open(relpath('../../criteria_map.json')))

# ---------------------------------------------------------------------------
# Expansions
expansions = {
    7: dict(
        name='Battle for Azeroth',
        short_name='BfA',
        max_level=120,
    ),
    6: dict(
        name='Legion',
        short_name='Leg',
        max_level=110,
    ),
    5: dict(
        name='Warlords of Draenor',
        short_name='WoD',
        max_level=100,
    ),
    4: dict(
        name='Mists of Pandaria',
        short_name='MoP',
        max_level=90,
    ),
    3: dict(
        name='Cataclysm',
        short_name='Cata',
        max_level=85,
    ),
    2: dict(
        name='Wrath of the Lich King',
        short_name='WotLK',
        max_level=80,
    ),
    1: dict(
        name='The Burning Crusade',
        short_name='TBC',
        max_level=70,
    ),
    0: dict(
        name='World of Warcraft',
        short_name='WoW',
        max_level=60,
    ),
}

# ---------------------------------------------------------------------------
# Weekly quest resets
weekly_resets = {
    # US realms (including Oceanic) reset Tuesday
    'us': 1,
    # EU realms reset Wednesday
    'eu': 2,
}

region_armory = {
    'us': 'en-us',
    'eu': 'en-gb',
    'kr': 'ko-kr',
}

# Keystones
keystone_maps = {
    197: ['EoA', 'Eye of Azshara'],
    198: ['DHT', 'Darkheart Thicket'],
    199: ['BRH', 'Black Rook Hold'],
    200: ['HoV', 'Halls of Valor'],
    206: ['NL', 'Neltharion\'s Lair'],
    207: ['VoW', 'Vault of the Wardens'],
    208: ['Maw', 'Maw of Souls'],
    209: ['Arc', 'The Arcway'],
    210: ['CoS', 'Court of Stars'],
    227: ['RKL', 'Return to Karazhan: Lower'],
    233: ['CEN', 'Cathedral of Eternal Night'],
    234: ['RKU', 'Return to Karazhan: Upper'],
    239: ['SoT', 'Seat of the Triumvirate'],
}

# Short difficulty names
difficulty_short = {
    3: '10 Normal',
    4: '25 Normal',
    5: '10 Heroic',
    6: '25 Heroic',
    7: 'LFR',
    14: 'Normal',
    15: 'Heroic',
    16: 'Mythic',
    17: 'LFR',
    23: 'Mythic',
}

# ---------------------------------------------------------------------------
# Profession cooldowns
profession_cooldowns = {
    # # Alchemy
    # 171: [
    #     dict(
    #         id=156587,
    #         name='Alchemical Catalyst',
    #         icon='inv_enchant_alchemycatalyst',
    #     ),
    #     dict(
    #         id=175880,
    #         name='Secrets of Draenor Alchemy',
    #         icon='inv_misc_book_08',
    #     ),
    # ],

    # # Blacksmithing
    # 164: [
    #     dict(
    #         id=171690,
    #         name='Truesteel Ingot',
    #         icon='inv_misc_trueironingot',
    #     ),
    #     dict(
    #         id=176090,
    #         name='Secrets of Draenor Blacksmithing',
    #         icon='inv_misc_book_11',
    #     ),
    # ],

    # # Enchanting
    # 333: [
    #     dict(
    #         id=169092,
    #         name='Temporal Crystal',
    #         icon='inv_enchanting_wod_crystalshard4',
    #     ),
    #     dict(
    #         id=177043,
    #         name='Secrets of Draenor Enchanting',
    #         icon='inv_misc_book_08',
    #     ),
    # ],
    # # Engineering
    # 202: [
    #     dict(
    #         id=169080,
    #         name='Gearspring Parts',
    #         icon='inv_eng_gearspringparts',
    #     ),
    #     dict(
    #         id=177054,
    #         name='Secrets of Draenor Engineering',
    #         icon='inv_misc_book_08',
    #     ),
    # ],
    # # Inscription
    # 773: [
    #     dict(
    #         id=169081,
    #         name='War Paints',
    #         icon='inv_inscription_warpaint_blue',
    #     ),
    #     dict(
    #         id=177045,
    #         name='Secrets of Draenor Inscription',
    #         icon='inv_misc_book_08',
    #     ),
    # ],
    # # Jewelcrafting
    # 755: [
    #     dict(
    #         id=170700,
    #         name='Taladite Crystal',
    #         icon='inv_jewelcrafting_taladitecrystal',
    #     ),
    #     dict(
    #         id=176087,
    #         name='Secrets of Draenor Jewelcrafting',
    #         icon='inv_misc_book_10',
    #     ),
    # ],
    # # Leatherworking
    # 165: [
    #     dict(
    #         id=171391,
    #         name='Burnished Leather',
    #         icon='inv_misc_startannedleather',
    #     ),
    #     dict(
    #         id=176089,
    #         name='Secrets of Draenor Leatherworking',
    #         icon='inv_misc_book_09',
    #     ),
    # ],
    # # Tailoring
    # 197: [
    #     dict(
    #         id=168835,
    #         name='Hexweave Cloth',
    #         icon='inv_tailoring_hexweavethread',
    #     ),
    #     dict(
    #         id=176058,
    #         name='Secrets of Draenor Tailoring',
    #         icon='inv_misc_book_03',
    #     ),
    # ],
}

all_cooldowns = set()
for spells in profession_cooldowns.values():
    for spell in spells:
        all_cooldowns.add(spell['id'])

# ---------------------------------------------------------------------------
# Bag slots
bag_slots = {}
for line in open(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../bag_slot.data'))):
    line = line.strip()
    if line:
        bagid, slots = line.split('=')
        bag_slots[int(bagid)] = int(slots)

bag_data = {
    1: [0, 1, 2, 3, 4],
    2: [-1, 5, 6, 7, 8, 9, 10, 11],
    3: [1, 2],
    5: [-3],
}

# ---------------------------------------------------------------------------
# Slot order
slot_order = [
    16,# main hand
    17,# off hand
    1, # head
    2, # neck
    3, # shoulder
    15,# back
    5, # chest
    #4, # shirt
    #19,# tabard
    9, # wrist
    10,# hands
    6, # waist
    7, # legs
    8, # feet
    11,# finger 1
    12,# finger 2
    13,# trinket 1
    14,# trinket 2
]

# ---------------------------------------------------------------------------
# Split lockout instances
split_lockouts = [
    # Mists of Pandaria
    6279, # Heart of Fear
    6125, # Mogu'shan Vaults
    6738, # Siege of Orgrimmar
    6067, # Terrace of Endless Spring
    6622, # Throne of Thunder

    # Warlords of Draenor
    6996, # Highmaul
    6967, # Blackrock Foundry
    7545, # Hellfire Citadel

    # Legion
    8026, # The Emerald Nightmare
    8440, # Trial of Valor
    8025, # The Nighthold
    8524, # Tomb of Sargeras
    8638, # Antorus, the Burning Throne
]

# Raid sort order override
raid_order_override = {
    # Legion
    8026: 5, # The Emerald Nightmare
    8440: 4, # Trial of Valor
    8025: 3, # The Nighthold
    8524: 2, # Tomb of Sargeras
    8638: 1, # Antorus, the Burning Throne
}

# ---------------------------------------------------------------------------
# Currency id:object map
currency_map = {}
for currency in Currency.objects.all():
    currency_map[currency.id] = currency

# ---------------------------------------------------------------------------
# Instance name:object map
instance_map = {}
for instance in Instance.objects.all():
    instance_map[instance.name] = instance

# ---------------------------------------------------------------------------
# Realm (region, name):object map
realm_map = {}
for realm in Realm.objects.all():
    realm_map[(realm.region, realm.name)] = realm
    realm_map[(realm.region, realm.name.lower())] = realm
    realm_map[realm.id] = realm

# ---------------------------------------------------------------------------
