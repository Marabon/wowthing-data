from .legion import *
from .warlords_of_draenor import *
from .mists_of_pandaria import *
from .cataclysm import *
from .wrath_of_the_lich_king import *
from .the_burning_crusade import *
from .vanilla import *

# ---------------------------------------------------------------------------
# Progress stuff
progress = [
    ['Legion', 'legion', progress_legion],
    ['Warlords of Draenor', 'wod', progress_warlords],
    ['Mists of Pandaria', 'mop', progress_pandaria],
    ['Cataclysm', 'cata', progress_cataclysm],
    ['Wrath of the Lich King', 'wotlk', progress_wrath],
    ['The Burning Crusade', 'tbc', progress_tbc],
    ['Vanilla', 'vanilla', progress_vanilla],
]

progress_data = {}
for duck in [progress_data_legion]:
    for k, v in duck.items():
        progress_data[k] = v

# ---------------------------------------------------------------------------

            # # Warrior
            # 1: [
            # ],
            # # Paladin
            # 2: [
            # ],
            # # Hunter
            # 3: [
            # ],
            # # Rogue
            # 4: [
            # ],
            # # Priest
            # 5: [
            # ],
            # # Death Knight
            # 6: [
            # ],
            # # Shaman
            # 7: [
            # ],
            # # Mage
            # 8: [
            # ],
            # # Warlock
            # 9: [
            # ],
            # # Monk
            # 10: [
            # ],
            # # Druid
            # 11: [
            # ],
            # # Demon Hunter
            # 12: [
            # ],
