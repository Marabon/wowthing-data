progress_wrath = [
    ['Stuff', [
        dict(
            name="The Argent Tournament",
            icon='achievement_reputation_argentcrusader',
            level=70,
            type='quest',
            alliance_things=[
                [[13679], "The Aspirant's Challenge"],
                [[13725], "The Valiant's Challenge [Darnassus]"],
                [[13724], "The Valiant's Challenge [Exodar]"],
                [[13723], "The Valiant's Challenge [Gnomeregan]"],
                [[13713], "The Valiant's Challenge [Ironforge]"],
                [[13699], "The Valiant's Challenge [Stormwind]"],
            ],
            horde_things=[
                [[13680], "The Aspirant's Challenge"],
                [[13726], "The Valiant's Challenge [Orgrimmar]"],
                [[13727], "The Valiant's Challenge [Sen'jin]"],
                [[13731], "The Valiant's Challenge [Silvermoon"],
                [[13728], "The Valiant's Challenge [Thunder Bluff]"],
                [[13729], "The Valiant's Challenge [Undercity]"],
            ],
        ),
    ]],
]
